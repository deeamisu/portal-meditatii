<?php

namespace App\Controller;

use App\Service\AdminService;
use App\Service\TeacherEventService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ActionsController extends AbstractController
{
    /**
     * @Route("/actions", name="actions")
     */
    public function index(AdminService $adminService)
    {
        return $this->render('actions/index.html.twig', [
            'students' => $adminService->getStudents(),
        ]);
    }

    /**
     * @Route("/actions/promote/{studentid}", name="actions_promote", methods={"GET"})
     */
    public function promoteStudent($studentid, AdminService $adminService)
    {
        $adminService->promoteStudent($studentid);
        return $this->redirectToRoute('actions');
    }

    /**
     * @Route("/actions/teachers", name="show_teachers", methods={"GET"})
     */
    public function showTeachers(AdminService $adminService)
    {
        return $this->render('actions/teachers.html.twig',[
            'teachers' => $adminService->getTeachers(),
        ]);
    }

    /**
     * @Route("/actions/inbox", name="teacher_inbox")
     */
    public function showTeacherInbox(AdminService $adminService)
    {
        return $this->render('actions/teacher_inbox.html.twig', [
            'bookings' => $adminService->getTeacherInbox()
        ]);
    }

    /**
     * @Route("/actions/confirm/booking/{id}", name="confirm_booking", methods={"GET"})
     */
    function confirmBooking($id, AdminService $adminService)
    {
        $adminService->teacherAcceptBooking($id);
        return $this->render('actions/teacher_inbox.html.twig',[
            'bookings' => $adminService->getTeacherInbox()
        ]);
    }

    /**
     * @Route("/actions/zoom/meeting", name="zoom_meeting")
     */
    public function createZoomMeeting(TeacherEventService $teacherEventService)
    {
        return $this->redirect($teacherEventService->createZoomMeeting()['start_url']);

        /**
        return $this->render('actions/zoom.html.twig',[
            'zoomMeeting' => $teacherEventService->createZoomMeeting(),
        ]);
        */
    }
}
