<?php

namespace App\Service;
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 8/25/2020
 * Time: 10:59 PM
 */
use App\Entity\Admin;
use App\Entity\Booking;
use App\Event\TeacherEvent;
use App\EventSubscriber\CalendarSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use App\Repository\BookingRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twilio\Rest\Client;
use Symfony\Component\Security\Core\Security;

class AdminService
{
    private $passwordEncoder;

    protected $entityManager;

    protected $bookingRepository;

    private $urlGenerator;

    private $security;

    protected $parameterBag;

    /**
     * AdminService constructor.
     * @param $passwordEncoder UserPasswordEncoderInterface
     * @param $entityManager EntityManagerInterface
     * @param $bookingRepository BookingRepository
     * @param $urlGenerator UrlGeneratorInterface
     * @param $security Security
     * @param $parameterBag
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $entityManager,
                                BookingRepository $bookingRepository, UrlGeneratorInterface $urlGenerator,
                                Security $security,ParameterBagInterface $parameterBag)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
        $this->bookingRepository = $bookingRepository;
        $this->urlGenerator = $urlGenerator;
        $this->security = $security;
        $this->parameterBag = $parameterBag;
    }

    public function encodePassword($admin, $password)
    {
        return $this->passwordEncoder->encodePassword($admin, $password);
    }

    public function getStudents()
    {
        return $this->entityManager->getRepository(Admin::class)->getStudents($this->entityManager);
    }

    public function promoteStudent($studentid)
    {
        $student = $this->entityManager->find(Admin::class, $studentid);
        /**
         * @var $student Admin
         */
        $student->setRoles(['ADMIN']);
        $this->entityManager->flush();
    }

    public function getTeachers()
    {
        return $this->entityManager->getRepository(Admin::class)->getTeachers($this->entityManager);
    }

    public function teacherDispatchEvent($id)
    {
        $dispatcher = new EventDispatcher();
        $event = new TeacherEvent($id, $this->entityManager,new TeacherEventService($this->passwordEncoder,$this->entityManager,
            $this->bookingRepository,$this->urlGenerator, $this->security, $this->parameterBag));
        $subscriber = new CalendarSubscriber($this->bookingRepository, $this->urlGenerator);
        $dispatcher->addSubscriber($subscriber);
        $dispatcher->dispatch($event, TeacherEvent::NAME);
    }

    public function getTeacher($id)
    {
        return $this->entityManager->find(Admin::class, $id);
    }

    /**
     * @param $booking Booking
     */
    public function sendSMS($booking)
    {
        // Your Account SID and Auth Token from twilio.com/console
        $account_sid = 'AC031a2fbcc4e2beef06cf64a98f7b227c';
        $auth_token = 'badc909a66462d0dabe6f35d6e53c50d';
        // In production, these should be environment variables. E.g.:
        // $auth_token = $_ENV["TWILIO_AUTH_TOKEN"]

        // A Twilio number you own with SMS capabilities
        $twilio_number = "+18123933644";

        $client = new Client($account_sid, $auth_token);
        $client->messages->create(
        // Where to send a text message (your cell phone?)
            $booking->getTeacher()->getPhoneNumber(),
            array(
                'from' => $twilio_number,
                'body' => 'Aveti o programare in asteptare !'
            )
        );
    }

    public function getTeacherInbox()
    {
        $teacher = $this->security->getUser();
        return $this->bookingRepository->findUnconfirmed($teacher, $this->entityManager);
    }

    public function teacherAcceptBooking($id)
    {
        /**
         * @var $booking Booking
         */
        $booking = $this->entityManager->find(Booking::class, $id);
        $booking->setConfirmed('confirmed');
        $this->entityManager->flush();
    }
}