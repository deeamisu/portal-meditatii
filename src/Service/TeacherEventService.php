<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 8/27/2020
 * Time: 12:30 AM
 */

namespace App\Service;

use App\Entity\Admin;
use Zoom\Meeting;

class TeacherEventService extends AdminService
{
    public function setActiveTeacher($id)
    {
        /**
         * @var $teacher Admin
         */
        $teacher = $this->entityManager->find(Admin::class,$id);
        $entries = $this->bookingRepository->findAll();
        foreach ($entries as $entry){
            $entry->setActiveTeacher(null);
            $this->entityManager->flush();
        }
        $bookings = $teacher->getTeacherBookings();
        foreach ($bookings as $booking){
            $booking->setActiveTeacher($id);
            $this->entityManager->flush();
        }
    }

    public function createZoomMeeting()
    {
        //$dir = $this->parameterBag->get('kernel.project_dir');
        //require_once $dir.'/Zoom/Index.php';
        //use Zoom\Meeting;

        $meeting = new Meeting();
        //create a data meeting
        $data = [
            'topic' => 'A new zoom meeting',
            'agenda' => 'our meeting desc',
            'settings' => [
                'host_video' => false,
                'participant_video' => true,
                'join_before_host' => true,
                'audio' => true,
                'approval_type' => 2,
                'waiting_room' => false,
    ],
];
        return $meeting->create($data);
    }
}