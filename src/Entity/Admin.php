<?php

namespace App\Entity;

use App\Repository\AdminRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=AdminRepository::class)
 */
class Admin implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\OneToMany(targetEntity=Booking::class, mappedBy="teacher")
     */
    private $teacherBookings;

    /**
     * @ORM\OneToMany(targetEntity=Booking::class, mappedBy="student")
     */
    private $studentBookings;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    public function __construct()
    {
        $this->bookings = new ArrayCollection();
        $this->teacherBookings = new ArrayCollection();
        $this->studentBookings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        //$roles[] = 'ROLE_ADMIN';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getName().' '.$this->getSurname();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @return Collection|Booking[]
     */
    public function getTeacherBookings(): Collection
    {
        return $this->teacherBookings;
    }

    public function addTeacherBooking(Booking $teacherBooking): self
    {
        if (!$this->teacherBookings->contains($teacherBooking)) {
            $this->teacherBookings[] = $teacherBooking;
            $teacherBooking->setTeacher($this);
        }

        return $this;
    }

    public function removeTeacherBooking(Booking $teacherBooking): self
    {
        if ($this->teacherBookings->contains($teacherBooking)) {
            $this->teacherBookings->removeElement($teacherBooking);
            // set the owning side to null (unless already changed)
            if ($teacherBooking->getTeacher() === $this) {
                $teacherBooking->setTeacher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Booking[]
     */
    public function getStudentBookings(): Collection
    {
        return $this->studentBookings;
    }

    public function addStudentBooking(Booking $studentBooking): self
    {
        if (!$this->studentBookings->contains($studentBooking)) {
            $this->studentBookings[] = $studentBooking;
            $studentBooking->setStudent($this);
        }

        return $this;
    }

    public function removeStudentBooking(Booking $studentBooking): self
    {
        if ($this->studentBookings->contains($studentBooking)) {
            $this->studentBookings->removeElement($studentBooking);
            // set the owning side to null (unless already changed)
            if ($studentBooking->getStudent() === $this) {
                $studentBooking->setStudent(null);
            }
        }

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
