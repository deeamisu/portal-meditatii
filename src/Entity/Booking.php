<?php

namespace App\Entity;

use App\Repository\BookingRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookingRepository::class)
 */
class Booking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $beginAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endAt = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity=Admin::class, inversedBy="teacherBookings")
     */
    private $teacher;

    /**
     * @ORM\ManyToOne(targetEntity=Admin::class, inversedBy="studentBookings")
     */
    private $student;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $activeTeacher;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $confirmed;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBeginAt(): ?\DateTimeInterface
    {
        return $this->beginAt;
    }

    public function setBeginAt(\DateTimeInterface $beginAt): self
    {
        $this->beginAt = $beginAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(?\DateTimeInterface $endAt = null): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTeacher(): ?Admin
    {
        return $this->teacher;
    }

    public function setTeacher(?Admin $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    public function getStudent(): ?Admin
    {
        return $this->student;
    }

    public function setStudent(?Admin $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getActiveTeacher(): ?int
    {
        return $this->activeTeacher;
    }

    public function setActiveTeacher(?int $activeTeacher): self
    {
        $this->activeTeacher = $activeTeacher;

        return $this;
    }

    public function getConfirmed(): ?string
    {
        return $this->confirmed;
    }

    public function setConfirmed(?string $confirmed): self
    {
        $this->confirmed = $confirmed;

        return $this;
    }
    
}
