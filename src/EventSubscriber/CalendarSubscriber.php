<?php

namespace App\EventSubscriber;

use App\Entity\Admin;
use App\Entity\Booking;
use App\Repository\BookingRepository;
use DateTimeInterface;
use CalendarBundle\CalendarEvents;
use CalendarBundle\Entity\Event;
use CalendarBundle\Event\CalendarEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Event\TeacherEvent;

class CalendarSubscriber implements EventSubscriberInterface
{
    private $bookingRepository;
    private $router;

    public function __construct(
        BookingRepository $bookingRepository,
        UrlGeneratorInterface $router
    ) {
        $this->bookingRepository = $bookingRepository;
        $this->router = $router;
    }

    public static function getSubscribedEvents()
    {
        return [
            CalendarEvents::SET_DATA => 'onCalendarSetData',
            TeacherEvent::NAME => 'onTeacherCalendar',
        ];
    }

    public function onCalendarSetData(CalendarEvent $calendar)
    {
        $start = $calendar->getStart();
        $end = $calendar->getEnd();
        $filters = $calendar->getFilters();

        // Modify the query to fit to your entity and needs
        // Change booking.beginAt by your start date property
        $bookings = $this->bookingRepository
            ->createQueryBuilder('booking')
            ->where('booking.beginAt BETWEEN :start and :end and booking.activeTeacher is not null and booking.confirmed is not null  
                OR booking.endAt BETWEEN :start and :end and booking.activeTeacher is not null and booking.confirmed is not null')
            ->setParameter('start', $start->format('Y-m-d H:i:s'))
            ->setParameter('end', $end->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getResult()
        ;

            /**
             * @var $booking Booking
             */
            foreach ($bookings as $booking) {
                // this create the events with your data (here booking data) to fill calendar
                $bookingEvent = new Event(
                    $booking->getTitle(),
                    $booking->getBeginAt(),
                    $booking->getEndAt() // If the end date is null or not defined, a all day event is created.
                );

                /*
                 * Add custom options to events
                 *
                 * For more information see: https://fullcalendar.io/docs/event-object
                 * and: https://github.com/fullcalendar/fullcalendar/blob/master/src/core/options.ts
                 */

                $bookingEvent->setOptions([
                    'backgroundColor' => 'red',
                    'borderColor' => 'red',
                ]);
                $bookingEvent->addOption(
                    'url',
                    $this->router->generate('booking_show', [
                        'id' => $booking->getId(),
                    ])
                );

                // finally, add the event to the CalendarEvent to fill the calendar
                $calendar->addEvent($bookingEvent);
            }
    }

    public function onTeacherCalendar(TeacherEvent $teacherEvent)
    {
        $teacherEvent->getId();
    }
}