<?php

namespace App\Event;

use App\Service\TeacherEventService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 8/26/2020
 * Time: 10:05 PM
 */
class TeacherEvent extends Event
{
    const NAME = 'teacher.selected';

    private $id;

    private $entityManager;

    private $teacherEventService;

    /**
     * TeacherEvent constructor.
     * @param $id
     * @param $entityManager
     * @param $teacherEventService
     */
    public function __construct($id, EntityManagerInterface $entityManager, TeacherEventService $teacherEventService)
    {
        $this->id = $id;
        $this->entityManager = $entityManager;
        $this->teacherEventService = $teacherEventService;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->teacherEventService->setActiveTeacher($this->id);
    }

}